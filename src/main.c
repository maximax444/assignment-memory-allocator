
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>


void* map_pages(void const* addr, size_t length, int additional_flags);
struct block_header* mem_block;
struct block_header* mem_block2;


static bool t1() {
    printf("\nТест 1: Обычное успешное выделение памяти\n");
    size_t test_size = 400;

    void *malloc = _malloc(test_size);
    debug_heap(stdout, mem_block);

    size_t capacity = mem_block->capacity.bytes;
    bool res = (capacity == test_size && malloc != NULL);
    _free(malloc);
    if (res) {
    	printf("Тест 1: Успешно\n");
    	return true;
    } else {
    	printf("Тест 1: Провалено\n");
    	return false;
    }
    
}
static size_t get_number_of_blocks(struct block_header *block) {
    size_t count = 0;

    while (block->next != NULL){
        count++;
        block = block->next;
    }

    return count;
}
static inline struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static bool t2() {
	printf("\nТест 2: Освобождение одного блока из нескольких выделенных\n");
    void *mem1 = _malloc(3000);
    void *mem2 = _malloc(500);

    debug_heap(stdout, mem_block);

    size_t block_number_before = get_number_of_blocks(mem_block);

    _free(mem2);
   
    size_t block_number_after = get_number_of_blocks(mem_block);

    _free(mem1);

    if((block_number_before - block_number_after) == 1){
        printf("Тест 2: Успешно\n");
    	return true;
    }else{
        printf("Тест 2: Провалено\n");
    	return false;
    } 
}


static bool t3() {
	printf("\nТест 3: Освобождение двух блоков из нескольких выделенных\n");

    void *mem1 = _malloc(3000);
    void *mem2 = _malloc(2000);
    void *mem3 = _malloc(1000);

    debug_heap(stdout, mem_block);

    
    _free(mem3);
    _free(mem2);
    
    size_t block_number_after = get_number_of_blocks(mem_block);

    _free(mem1);
    

    if (block_number_after == 1) {
        printf("Тест 3: Успешно\n");
    	return true;
    }else{
        printf("Тест 3: Провалено\n");
    	return false;
    }
}

static bool t4() {
	printf("\nТест 4: Память закончилась, новый регион памяти расширяет старый\n");


    void *mem1 = _malloc(5000);
    void *mem2 = _malloc(4000);
    void *mem3 = _malloc(3000);

    debug_heap(stdout, mem_block);

    size_t block_number = get_number_of_blocks(mem_block);

    if (block_number == 3) {
        printf("Тест 4: Успешно\n");
        _free(mem1);
        _free(mem2);
        _free(mem3);
    	return true;
    } else {
    	printf("Тест 4: Провалено\n");
    	_free(mem1);
    	_free(mem2);
    	_free(mem3);
    	return false;
    }

    
}


#define HEAP_SIZE 5000
#define HUGE 50000
static bool t5() {
    printf("\nТест 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов\n");

    void *malloc_1 = _malloc(12000);
    struct block_header *block_1 = block_get_header(malloc_1);
    debug_heap(stdout, block_1);
    struct block_header * next_block = block_1->next;
    void*  tmp = next_block->contents + next_block->capacity.bytes;
    void*  mtmp = mmap(tmp, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (mtmp == NULL) {
    	return false;
    }
    void *malloc_2 = _malloc(7000);
    debug_heap(stdout, block_1);
    struct block_header *block_2 = block_get_header(malloc_2);
    bool check_1 = !block_1->is_free;
    bool check_2 = !block_2->is_free;
    if (check_1 && check_2) {
        _free(malloc_1);
        _free(malloc_2);
        debug_heap(stdout, block_1);
    }




    printf("Тест 5: Успешно\n");
    return true;

}



bool test_all() {

	mem_block = (struct block_header *) heap_init(10000);
	mem_block2 = (struct block_header *) heap_init(5000);
	if (mem_block == NULL) {
	    printf("Heap init failed\n");
	    exit(1);
	}
    bool res = true;
    for (size_t i = 1; i < 6; i++) {
        printf("Тест %zu\n", i);
        bool iter_res = false;
        if (i==1) {
        	iter_res = t1();
        }
        if (i==2) {
        	iter_res = t2();
        }
        if (i==3) {
        	iter_res = t3();
        }
        if (i==4) {
        	iter_res = t4();
        }
        if (i==5) {
        	iter_res = t5();
        }
        if (iter_res) {
            printf("\nТест %zu пройден\n", i);
        } else {
            printf("\nТест %zu не пройден\n", i);
            res = false;
        }

    }

    return res;
}

int main() {


    if (test_all()) {
    	printf("Все мои тесты пройдены! \n");
    	return 0;
        
    } else {
    	printf("Тесты провалены\n");
    	return 1;
    }

    
}
